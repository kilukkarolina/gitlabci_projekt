import unittest
import os
from selenium import webdriver

class SmokeTests(unittest.TestCase):

    def setUp(self):
        if os.name == 'nt':
            self.driver = webdriver.Chrome(executable_path=r"libs\chromedriver.exe")
        else:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            self.driver = webdriver.Chrome(options=chrome_options)

        print('setUp')
        self.base_url = 'https://statusgen.testoneo.com/'
        self.test_data_dir = 'testResults'

    def tearDown(self):
        print('tearDown')
        self.driver.quit()

    def test_status_worker_page_title(self):
        self.driver.get(self.base_url)
        expected_title = 'Status Worker'
        actual_title = self.driver.title
        # self.driver.get_screenshot_as_file("testResults/screenshot.png")
        self.assertEqual(expected_title, actual_title)