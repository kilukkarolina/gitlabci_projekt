import unittest
import os
from selenium import webdriver

class StatusTests(unittest.TestCase):

    def setUp(self):
        if os.name == 'nt':
            self.driver = webdriver.Chrome(executable_path=r"libs\chromedriver.exe")
        else:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            self.driver = webdriver.Chrome(options=chrome_options)

        print('setUp')
        self.base_url = 'https://statusgen.testoneo.com/'

    def tearDown(self):
        print('tearDown')
        self.driver.quit()

    def test_minute_status(self):
        self.driver.get(self.base_url)
        status = ['statusM']
        for value in status:
            actual_value = self.driver.find_element_by_id(value).text
            print(value)
            expected_value = "ON"
            self.driver.get_screenshot_as_file("testResults/screenshot.png")
            self.assertEqual(expected_value, actual_value)