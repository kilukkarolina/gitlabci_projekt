import time
import unittest
import os
from selenium import webdriver

class ToggleTests(unittest.TestCase):

    def setUp(self):
        if os.name == 'nt':
            self.driver = webdriver.Chrome(executable_path=r"libs\chromedriver.exe")
        else:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            self.driver = webdriver.Chrome(options=chrome_options)

        print('setUp')
        self.base_url = 'https://statusgen.testoneo.com/'
        self.test_data_dir = 'testResults'
        time.sleep(1)

    def tearDown(self):
        print('tearDown')
        self.driver.quit()

    def test_toggle_changed_to_disabled(self):
        self.driver.get(self.base_url)
        toggle = self.driver.find_element_by_xpath('//*[@data-toggle="toggle"]')
        toggle.click()
        time.sleep(1)
        status = ['statusS', 'statusM', 'statusH', 'statusD', 'statusMM', 'statusY', 'statusQ', 'statusHaH', 'status12H']
        for value in status:
            actual_value = self.driver.find_element_by_id(value).text
            expected_value = "unknown"
            self.assertEqual(expected_value, actual_value)
